package konsolenausgabe;
import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("R�mische Zahl:");
		String rZahl = tastatur.next();
		char rEinzel = 0, prevEinzel = 0;
		int zahl = 0, repCtrl = 1;
		
		tastatur.close();

		for(int i = 0;i <= rZahl.length() - 1; i++) {
			rEinzel = rZahl.charAt(i);
			if(rEinzel == prevEinzel) {
				repCtrl += 1;
			}
			if(rEinzel == prevEinzel && (rEinzel == 'V' || rEinzel == 'L' || rEinzel == 'D')) {
				repCtrl = 4;
			}
			if((rEinzel == 'V' || rEinzel == 'X') && prevEinzel == 'I') {
				zahl -= 2;
			}
			if((rEinzel == 'L' || rEinzel == 'C') && prevEinzel == 'X') {
				zahl -= 20;
			}
			if((rEinzel == 'D' || rEinzel == 'M') && prevEinzel == 'C') {
				zahl -= 200;
			}
			switch(rEinzel){
				case 'I':
					zahl += 1;
					break;
				case 'V':
					zahl += 5;
					break;
				case 'X':
					zahl += 10;
					break;
				case 'L':
					zahl += 50;
					break;
				case 'C':
					zahl += 100;
					break;
				case 'D':
					zahl += 500;
					break;
				case 'M':
					zahl += 1000;
					break;
			}
			prevEinzel = rEinzel;
		}
		if (repCtrl > 3) {
			System.out.println("Keine G�ltige R�mische Zahl, es d�rfen max. drei von den selben Zeichen folgen!\n"
							 + "V, L und D d�rfen niemals mehrfach nebeneinander stehen");
		}else {
			System.out.println("= " + zahl);
		}
	}

}
