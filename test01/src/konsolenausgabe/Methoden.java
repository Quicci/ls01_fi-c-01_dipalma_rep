package konsolenausgabe;

public class Methoden {
	
	public static double mittelwert(double...x) {
		int y = 0;
		double zwichenstand = 0;
		while(y < x.length) {
			zwichenstand += x[y];
			y++;
		}
		return  zwichenstand/x.length;
	}
	
	public static double quadrat(double x) {
		return x*x;
	}
	
	public static double hypothenuse(double k1, double k2) {
		return Math.sqrt(quadrat(k1) + quadrat(k2));
	}

	public static double berechneKreisfläche(double r) {
		return r * r * Math.PI;
	}

	static double berechneReihenschaltung(double r1, double r2) {
		return r1 + r2;
	}

	static double berechneParallelschaltung(double r1, double r2) {
		return 1.0 / (1.0 / r1 + 1.0 / r2);
	}
	
	static double beschleunigen(double v, double dv) {
		if(v + dv <= 130 && v + dv >= 0) {
			v += dv;
		}else {
			if(v + dv < 0) {
				v = 0;
			}else {
				v = 130;
			}
		}
		return v;
	}

	public static void main(String[] args) {
		int[] zahlen = new int[] {1,2,3,4,5};
		
		for(int i = 1; i < zahlen.length; i += 2) {
			System.out.println("Ausgabe");
		}
	}
	
}
