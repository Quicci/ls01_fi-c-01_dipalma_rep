package konsolenausgabe;

import java.util.Scanner;

public class Fahrsimulator {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double v = 0;
		do {
			System.out.printf("Geschwindigkeit: %3.0f km/h \nBeschleunigen um: ", v);
			double geschwindigkeit = tastatur.nextDouble();
			v = Methoden.beschleunigen(v, geschwindigkeit);
		} while (v > 0);
		System.out.println("Sie sind zum stopp gekommen, um die Fahrt fortzufahren, starten sie das Programm erneut");
	}

}
