package konsolenausgabe;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Twister {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		String text = tastatur.nextLine(), textErg = "";
		tastatur.close();
		int iStart = 0, iWord = 0;
		String[] word = new String[text.length()];
		for(int i = 0; i < text.length(); i++) {
			if(text.charAt(i) == ' ') {
				if(text.charAt(i - 1) == '.') {
					word[iWord] = text.substring(iStart, i - 1);
					word[iWord + 1] = ".";
				}else {
					word[iWord] = text.substring(iStart, i);
					word[iWord + 1] = " ";
				}
				iWord += 2;
				iStart = i + 1;
			}
			if(i == text.length() - 1) {
				if(text.charAt(i) == '.') {
					word[iWord] = text.substring(iStart, i);
					word[iWord + 1] = ".";
				}else {
					word[iWord] = text.substring(iStart, i);
				}
			}
		}
		if(iWord == 0) {
			if(text.charAt(text.length() - 1) == '.') {
				word[0] = text.substring(iStart, text.length() - 1);
				word[1] = ".";
			}else {
				word[0] = text;
			}
		}
		for(int i = 0; i < word.length; i++) {
			if(word[i] == null) {break;}
			textErg += Twist(word[i]);
		}
		System.out.println(textErg);
	}
	
	public static String Twist(String word) {
		String erg = "";
		if(word.length() > 4){
			String inbetween, beg = word.substring(0, 1), end = word.substring(word.length() - 1, word.length());
			char[] letters = new char[word.length() - 2];
			char letter, copy, tmp;
			int rnd;
			inbetween = word.substring(1, word.length() - 1);
			for(int i = 0; i < inbetween.length(); i++) {
				letter = inbetween.charAt(i);
				letters[i] = letter;
			}
			for(int i = 0; i < letters.length; i++) {
				rnd = ThreadLocalRandom.current().nextInt(0, letters.length);
				copy = letters[i];
				tmp = letters[rnd];
				letters[rnd] = copy;
				letters[i] = tmp;
			}
			erg = beg;
			for(int i = 0; i < letters.length; i++) {
				erg += letters[i];
			}
			erg += end;
		}else if(word.length() == 4){
			String inbetween, beg = word.substring(0, 1), end = word.substring(word.length() - 1, word.length());
			char[] letters = new char[word.length() - 2];
			char letter, copy, tmp;
			int rnd;
			inbetween = word.substring(1, word.length() - 1);
			for(int i = 0; i < inbetween.length(); i++) {
				letter = inbetween.charAt(i);
				letters[i] = letter;
			}
			copy = letters[1];
			tmp = letters[2];
			letters[2] = copy;
			letters[1] = tmp;
			erg = beg;
			for(int i = 0; i < letters.length; i++) {
				erg += letters[i];
			}
			erg += end;
		}else {erg = word;}
		return erg;
	}

}
