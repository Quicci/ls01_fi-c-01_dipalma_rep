package konsolenausgabe;

public class Temperaturtabelle {
	public static void main(String[] args) {
		System.out.printf("%-12s| %10s\n", "Fahrenheit", "Celsius");
		System.out.println("------------------------");
		int[] f = { -20, -10, 0, 20, 30 };
		Double[] c = { -28.8889, -23.3333, -17.7778, -6.6667, -1.1111 };
		Integer x = 0;
		while (x < 5) {
			System.out.printf("%+-12d| %+10.2f\n", f[x], c[x]);
			x += 1;
		}
	}
}
