﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		String eingabe = "";
		boolean nochmal = false;
		double rückgabebetrag = 0;
		String[] einzelKarten = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", 
								 "Kurzstrecke"};
		double[] einzelPreise = {2.9,3.3,3.6,1.9};
		String[] tagesKarten = {"Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC"};
		double[] tagesPreise = {8.6,9,9.6};
		String[] gruppenKarten = {"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", 
								  "Kleingruppen-Tageskarte Berlin ABC"};
		double[] gruppenPreise = {23.5,24.3,24.9};
		do {
			nochmal = false;
			
			// Ticketbestellung
			// ----------------
			do {
				nochmal = false;
				System.out.printf("Einzelfahrscheine (1)\nTageskarten (2)\nGruppenkarten (3)\nBezahlen "
								+ "[%.2f EUR] (9)\nEingabe: ", rückgabebetrag);
					eingabe = tastatur.next();
					switch(eingabe.charAt(0)) {
						case '1':
							rückgabebetrag += fahrkartenbestellungErfassung(einzelKarten, einzelPreise);
							nochmal = true;
							break;
						case '2':
							rückgabebetrag += fahrkartenbestellungErfassung(tagesKarten, tagesPreise);
							nochmal = true;
							break;
						case '3':
							rückgabebetrag += fahrkartenbestellungErfassung(gruppenKarten, gruppenPreise);
							nochmal = true;
							break;
						case '9':
							if(rückgabebetrag > 0) {
								nochmal = false;
							}else {
								System.out.println("Sie haben noch keinen Artikel zum kaufen ausgewählt! "
												 + "Bitte wählen sie erstmal einen Artikel aus.");
								nochmal = true;
							}
							break;
						default:
							System.out.println("Bitte geben sie eine der möglichen Eingaben an!(1, 2, 3 oder 9)");
							nochmal = true;
							break;
					}
			}while(nochmal);
			
			nochmal = false;
			
			// Fahrscheinbezahlung
			// -------------------
			fahrkertenBezahlung(rückgabebetrag);
			
			// Fahrscheinausgabe
			// -----------------
			FahrkartenAusgabe();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(rückgabebetrag);

			// Erneute Bestellung oder Ende
			// ----------------------------
			System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wollen sie noch einen Kauf tätigen? (Ja/Nein)");
			eingabe = tastatur.next();
			if (eingabe.charAt(0) == 'J'||eingabe.charAt(0) == 'j') {
				nochmal = true;
			}else {
				System.out.println("Eine schöne Fahrt und einen schönen Tag noch :)");
			}
		} while (nochmal);
	}
	
	// ------------------------------Funktionen---------------------------------
	
	//Kümmert sich um das Menü, die Ticketauswahl und gibt den zu zahlenden Wert für den Einkauf zurück
	//Returnt den zu zahlenden Betrag
	public static double fahrkartenbestellungErfassung(String[] karten, double[] preise) {
		double zuZahlenderBetrag = 0;
		byte ticketAnzahl;
		int auswahl;

		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wählen sie bitte eins der folgenden Tickets aus:");
		for(int i = 1; i <= karten.length; i++) {
			System.out.printf("%-34s [%5.2f EUR] (%2d)\n", karten[i - 1], preise[i-1], i);
		}
		System.out.print("Auswahl: ");
		do {
			auswahl = tastatur.nextInt() - 1;
			if(auswahl <= karten.length || auswahl > 0) {
				zuZahlenderBetrag = preise[auswahl];
			}else {
				System.out.println("Bitte wählen sie eins von den möglichen Angeboten aus! \n"
						+ "Zum Auswählen geben sie bitte die Zahl in der Klammer aus");
			}
		}while(auswahl >= karten.length || auswahl < 0);
		System.out.print("Anzahl der Tickets: ");
		ticketAnzahl = tastatur.nextByte();
		while(ticketAnzahl < 1 || ticketAnzahl > 10) {
			if(ticketAnzahl < 1 || ticketAnzahl > 10) {
				System.out.println("Sie können leider nur 1-10 Tickets gleichzeitig kaufen."
								 + "\nBitte geben sie eine neue Ticketanzahl ein:");
				ticketAnzahl = tastatur.nextByte();
			}
		}
		zuZahlenderBetrag *= ticketAnzahl;

		return zuZahlenderBetrag;
	}

	//Kümmert sich um die Zahlung und errechnet den Rückgabebetrag
	//Returnt den Rückgabebetrag
	public static double fahrkertenBezahlung(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("Noch zu zahlen: "
					+ String.format("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag)) + " Euro");
			System.out.print("Eingabe (mind. 5Ct): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag + 0.00000001;
	}

	//Simuliert die Pausen zwischen den Fahrkartendruck
	public static void warte(int z) {
		try {
			Thread.sleep(z);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//Simuliert den Fahrkartendruck
	public static void FahrkartenAusgabe() {
		System.out.println("\nFahrschein/e wird/werden ausgegeben");

		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}
	
	//Stellt die einzelne Rückgabewerte als Münzen dar
	public static void muenzeAusgabe(int betrag, String einheit) {
		System.out.printf("    * * *\n"
						+ " *         *\n"
						+ "*%6d     *\n"
						+ "*%7s    *\n"
						+ " *         *\n"
						+ "    * * *\n", betrag, einheit);
	}

	//Berechnet dei Rückgabe in einzelne Münzen
	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag >= 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", rückgabebetrag) + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgabe(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgabe(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgabe(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgabe(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgabe(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgabe(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}
}