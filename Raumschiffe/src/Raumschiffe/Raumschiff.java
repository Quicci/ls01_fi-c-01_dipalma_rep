package Raumschiffe;
import java.util.ArrayList;

/**
 * 
 * Diese Klasse modeliert ein Startrek Raumschiff
 * @author Riccardo Di Palma
 * @version 1.0 vom 16.04.2021
 */

public class Raumschiff {

	private String name;
	private int photonentorpedoAnzahl;
	private int energieversorgungProzent;
	private int schildeProzent;
	private int huelleProzent;
	private int lebensSysProzent;
	private int androidAnzahl;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	/**
	 * Vollparametisierter Konstruktor f�r die Klasse Raumschiff
	 * @param name 							Name des Raumschiffes
	 * @param photonentorpedoAnzahl 		Anzahl der Photonentorpedos
	 * @param energieversorgungProzent		Energieversorgung des Raumschiffs in Prozent (0-100)
	 * @param schildeProzent				Integrit�t der Schilde des Raumschiffs in Prozent (0-100)
	 * @param huelleProzent					Integrit�t der Huelle des Raumschiffs in Prozent (0-100)
	 * @param lebensSysProzent				Erhaltung der Lebenssysteme des Raumschiffes in Prozent (0-100)
	 * @param androidAnzahl					Anzahl der Reperaturandroiden auf dem Raumschiff
	 */
	public Raumschiff(	String name, int photonentorpedoAnzahl, int energieversorgungProzent,
						int schildeProzent, int huelleProzent, int lebensSysProzent, int androidAnzahl) {
		this.name = name;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.androidAnzahl = androidAnzahl;
		this.energieversorgungProzent = energieversorgungProzent;
		this.schildeProzent = schildeProzent;
		this.huelleProzent = huelleProzent;
		this.lebensSysProzent = lebensSysProzent;
	}
	
	/**
	 * Mit der shoot Methode schie�t ein Raumschiff auf ein anderes Raumschiff
	 * @param tgt 	Das Raumschiffobjekt auf das geschossen wird
	 * @param ammo 	Munitionstyp, Auswahlm�glichkeit aus "Photon" oder "Phaser"
	 * @param menge Gibt die Menge der Munition an, gilt nur f�r Photonentorpedos
	 */
	public void shoot(Raumschiff tgt, String ammo /*"Photon" oder "Phaser"*/, int menge) {
		switch(ammo){
			case "Photon":
				if(this.photonentorpedoAnzahl > 0) {
					for(int i = 1; i <= getPhotonentorpedoAnzahl(); i++) {
						broadcast(this.name + " [Photonentorpedo] " + tgt.getName());
						hit(tgt);
					}
				}else {
					System.out.println("Keine Photonentorpedos geladen!\n");
					broadcast(this.name + " [-=*Click*=-] " + tgt.getName());
				}
				setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - menge);
				break;
			case "Phaser":
				
				if(this.energieversorgungProzent >= 50) {
					this.energieversorgungProzent -= 50;
					broadcast(this.name + " [Phaserkanone] " + tgt.getName());
					hit(tgt);
				}else {
					broadcast(this.name + " [-=*Click*=-] " + tgt.getName());
				}
				break;
			default:
				
				broadcast("Es konnte nichts abgefeuert werden, da der Munitionstyp \"" + ammo + "\" nicht existiert\n");
		}
	}
	
	/**
	 * laedt Photonentorpedos aus dem Lager in die photonentorpedoAnzahl
	 * @param menge Anzahl an Photonentorpedos die aus der Ladung genommen werden sollen
	 */
	public void loadPhoton(int menge) {
		for(Ladung elem : this.ladungsverzeichnis) {
			if(elem.getName() == "Photonentorpedos") {
				if(menge > elem.getMenge())	menge = elem.getMenge();
				elem.setMenge(elem.getMenge() - menge);
				setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + menge);
				System.out.println("[" + menge + "] Photonentorpedo(s) eingesetzt");
			}else {
				System.out.println("Keine Photonentorpedos gefunden!");
				broadcast("-=*Click*=-");
			}
		}
	}
	
	/**
	 * Die Hit 	Methode registriert einen Treffer auf ein Raumschiff und verrechnet diesen auf die Schilde 
	 * oder die H�lle und die Energieversorgung oder auf die Lebenserhaltungssysteme des Raumschiffs
	 * @param tgt 	Das Ziel-Raumschiff-Objekt
	 */
	private void hit(Raumschiff tgt) {
		int rest;
		broadcast(tgt.getName() + " wurde getroffen!");
		
		rest = Math.abs(tgt.getSchildeProzent())/2;
		
		if(rest <= 0) rest = 50;
		
		tgt.setSchildeProzent(tgt.getSchildeProzent() - 50);
		
		if(tgt.getSchildeProzent() < 49) {
			tgt.setHuelleProzent(tgt.getHuelleProzent() - rest);
			tgt.setEnergieversorgungProzent(tgt.getEnergieversorgungProzent() - rest);
			
			if(tgt.getHuelleProzent() <= 0) {
				tgt.setLebensSysProzent(0);
				broadcast("Die Lebenserhaltungssysteme vom Schiff " + tgt.getName() + " wurden zerst�rt!");
			}
			
		}
	}
	
	/**
	 * Repariert die Angegebenen Schiffstrukturen mit der Menge angegebener Reperatur Androiden, welche nach einer Zufallszahl effizient arbeiten
	 * @param androidMenge Die Menge an Androiden die arbeiten sollen
	 * @param schild Auswahl ob das Schild repariert werden soll
	 * @param huelle Auswahl ob die Huelle repariert werden soll
	 * @param energieVersorgung Auswahl ob die Energieversorgung repariert werden soll
	 */
	public void repair(int androidMenge, boolean schild, boolean huelle, boolean energieVersorgung) {
		
		if(androidMenge <= 0) {
			System.out.println("Es wurden keine oder 0 Androiden zugeteilt; [Aufgabe abgebrochen]");
		}
		if(!schild && !huelle && !energieVersorgung){
			System.out.println("Es wurden keine Einsatzgebiete festgelegt; [Aufgabe abgebrochen]");
		}
		
		int rng = (int)Math.floor(Math.random()*(100+1)+100), trueStruktur = 0, repEffizienz = 0;
		
		if(androidMenge > this.androidAnzahl) {
			androidMenge = this.androidAnzahl;
		}
		
		setAndroidAnzahl(getAndroidAnzahl() - androidMenge); 
		
		if(schild)				trueStruktur++;
		if(huelle)				trueStruktur++;
		if(energieVersorgung)	trueStruktur++;
		
		repEffizienz = (rng * androidMenge) / trueStruktur;
		
		if(schild)				setSchildeProzent(repEffizienz + this.schildeProzent);;
		if(huelle)				setHuelleProzent(repEffizienz + this.huelleProzent);;
		if(energieVersorgung)	setEnergieversorgungProzent(repEffizienz + this.energieversorgungProzent);;
	}
	
	/**
	 * Gibt eine Nachricht an den Broadcast Kommunikator des Raumschiffs und in die Konsole aus
	 * @param msg Die Nachricht die an den Broadcast Kommunikator des Raumschiffs weiter gegeben und in die Konsole ausgegeben wird
	 */
	public void broadcast(String msg) {
		broadcastKommunikator.add(msg);
		System.out.println("\n" + msg);
	}
	
	/**
	 * Gibt den gesmmten Broadcast Kommunikator eines Schiffes aus
	 */
	public void report() {
		for(String msg : this.broadcastKommunikator) {
			System.out.println(msg + "\n");
		}
	}
	
	public void status() {
		System.out.println("--------------------------------------------------------------------------------------");
		System.out.println(	"Raumschiff: " + getName() + "\nPhotonentorpedos: " + getPhotonentorpedoAnzahl() + 
							"\nEnergie bei " + getEnergieversorgungProzent() + "%\nSchilde bei " + getSchildeProzent() + 
							"%\nH�lle bei " + getHuelleProzent() + "%\nLebenserhaltungssysteme bei " + getLebensSysProzent() + 
							"%\nReperatur Androiden: " + getAndroidAnzahl());
		System.out.println("--------------------------------------------------------------------------------------\n");
	}
	
	/**
	 * F�gt ein Ladungsobjekt dem Laddungsverzeichnis des Raumschiffs hinzu
	 * @param ladung Das Ladungsobjekt, welches in das Ladungsverzeichnis des Raumschiffes aufgenommen werden soll
	 */
	public void addLadung(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}
	
	/**
	 * Gibt das komplette Ladungsverzeichnis mit Name und Menge einer jeden Ladung eines Raumschiffes aus
	 */
	public void ladungsreport() {
		System.out.println("Ladungsverzeichnis:");
		for(Ladung Elem : this.ladungsverzeichnis) {
			System.out.println("Ladungsinhalt: " + Elem.getName() + " x" + Elem.getMenge());
		}
		System.out.println();
	}
	
	/**
	 * R�umt das Ladungsverzeichnis auf, indem es Ladungen entfernt, die eine Menge von 0 haben
	 */
	public void cleanup() {
		for(Ladung elem : this.ladungsverzeichnis) {
			if(elem.getMenge() == 0) {
				this.ladungsverzeichnis.remove(elem);
			}
		}
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	/**
	 * Sorgt daf�r, dass photonentorpedoAnzahl nicht unter 0 gehen kann
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		if(this.photonentorpedoAnzahl < 0)	this.photonentorpedoAnzahl = 0;
	}

	public int getAndroidAnzahl() {
		return androidAnzahl;
	}
	public void setAndroidAnzahl(int androidAnzahl) {
		this.androidAnzahl = androidAnzahl;
	}
	
	public int getEnergieversorgungProzent() {
		return energieversorgungProzent;
	}
	/**
	 * Verhindert eine Energieversorgung von unter 0% und �ber 100%
	 * @param energieversorgungProzent
	 */
	public void setEnergieversorgungProzent(int energieversorgungProzent) {
		this.energieversorgungProzent = energieversorgungProzent;
		if(this.energieversorgungProzent < 0) {
			this.energieversorgungProzent = 0;
		}else if(this.energieversorgungProzent > 100) {
			this.energieversorgungProzent = 100;
		}
	}
	
	public int getSchildeProzent() {
		return schildeProzent;
	}
	/**
	 * Verhindert eine Schildintegrit�t von unter 0% und �ber 100%
	 * @param schildeProzent
	 */
	public void setSchildeProzent(int schildeProzent) {
		this.schildeProzent = schildeProzent;
		if(this.schildeProzent < 0) {
			this.schildeProzent = 0;
		}else if(this.schildeProzent > 100) {
			this.schildeProzent = 100;
		}
	}
	
	public int getHuelleProzent() {
		return huelleProzent;
	}
	/**
	 * Verhindert eine H�llenintegrit�t von unter 0% und �ber 100%
	 * @param huelleProzent
	 */
	public void setHuelleProzent(int huelleProzent) {
		this.huelleProzent = huelleProzent;
		if(this.huelleProzent < 0) {
			this.huelleProzent = 0;
		}else if(this.huelleProzent > 100) {
			this.huelleProzent = 100;
		}
	}
	
	public int getLebensSysProzent() {
		return lebensSysProzent;
	}
	public void setLebensSysProzent(int lebensSysProzent) {
		this.lebensSysProzent = lebensSysProzent;
	}
	
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
}