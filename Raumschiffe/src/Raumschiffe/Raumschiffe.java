package Raumschiffe;

import java.util.ArrayList;

public class Raumschiffe {

	public static void main(String[] args) {
		int min = 0, max = 100;
		int rngEnrg = (int)Math.floor(Math.random()*(max-min+1)+min);
		int rngShld = (int)Math.floor(Math.random()*(max-min+1)+min);
		int rngShll = (int)Math.floor(Math.random()*(max-min+1)+min);
		
		Raumschiff Klingonen = new Raumschiff("IKS Hegh'ta", 0, 100, 50, 100, 100, 2);
		Ladung Kling1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung Kling2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Klingonen.addLadung(Kling1);
		Klingonen.addLadung(Kling2);
		
		Raumschiff Romulaner = new Raumschiff("IRW Khazara", 2, 50, 50, 100, 100, 2);
		Ladung Romul1 = new Ladung("Borg-Schrott", 5);
		Ladung Romul2 = new Ladung("Rote Materie", 2);
		Ladung Romul3 = new Ladung("Plasma-Waffe", 50);
		Romulaner.addLadung(Romul1);
		Romulaner.addLadung(Romul2);
		Romulaner.addLadung(Romul3);
		
		Raumschiff Vulkanier = new Raumschiff("Ni'Var", 3, rngEnrg, rngShld, rngShll, 100, 5);
		Ladung Vulka1 = new Ladung("Forschungssonde", 35);
		Vulkanier.addLadung(Vulka1);
		
		Raumschiff Schiffe[] = {Klingonen, Romulaner, Vulkanier};
		for(int i = 0; i <= 2; i++){
			Schiffe[i].status();
			Schiffe[i].ladungsreport();
		}
		
		Vulkanier.shoot(Romulaner, "Photon", 3);
		Romulaner.status();
		Romulaner.repair(2, false, true, true);
		Romulaner.status();
	}
}
