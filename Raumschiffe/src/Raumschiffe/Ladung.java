package Raumschiffe;

/**
 * 
 * Diese Klasse modeliert eine Ladung f�r ein Startrek Raumschiff
 * @author Riccardo Di Palma
 * @version 1.0 vom 16.04.2021
 */

public class Ladung {

	private String name;
	private int menge;
	
	/**
	 * Vollparametisierter Konstruktor der Ladungsklasse
	 * @param name	Titel des Ladungsinhalts
	 * @param menge	Menge des Ladungsinhalts
	 */
	public Ladung(String name, int menge) {
		this.name = name;
		this.menge = menge;
	}
	
	
	/**
	 * Unparametisierter Konstrukter der Ladungsklasse
	 */
	public Ladung() {
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
}
