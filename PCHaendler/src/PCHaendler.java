import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		String produkt;
		int anzahl;
		double preis, mwst, netto, brutto;
		produkt = liesString("Was m�chten Sie bestellen? ");
		anzahl = liesInt("Geben Sie die Anzahl ein: ");
		preis = liesDouble("Geben Sie den Nettopreis ein: ");
		mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
		netto = Verarbeiten(true, anzahl, preis, mwst);
		brutto = Verarbeiten(false, anzahl, preis, mwst);
		
		Ausgabe(produkt, anzahl, netto, brutto, mwst);
	}
	
	public static String liesString(String text) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println(text);
		return tastatur.next();
	}
	
	public static int liesInt(String text) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println(text);
		return tastatur.nextInt();
	}
	
	public static double liesDouble(String text) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println(text);
		return tastatur.nextDouble();
	}
	
	public static double Verarbeiten(boolean N, int anzahl, double preis, double mwst) {
		double netto = anzahl * preis;
		double brutto = netto * (1 + mwst  / 100);
		if(N){ return netto; }else { return brutto; }
	}
	
	public static void Ausgabe(String produkt, int anzahl, double netto, double brutto, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d St�ck %10.2f � %n", produkt, anzahl, netto);
		System.out.printf("\t\t Brutto: %-20s %6d St�ck %10.2f � (%.1f%s)%n", produkt, anzahl, brutto, mwst, "%");
	}

}
